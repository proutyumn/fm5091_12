﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FM5091_LastClass
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            // define some output variables for the text box values
            double v1, v2;
            // use tryparse to figure out whether the user entered value numbers
            if(double.TryParse(txtValue1.Text, out v1) && 
                double.TryParse(txtValue2.Text, out v2))
            {
                // if the user data is legit, do the calculation
                if(radioAdd.Checked)
                {
                    lblResult.Text = (v1 + v2).ToString();
                }
                else if (radioSubtract.Checked)
                {
                    lblResult.Text = (v1 - v2).ToString();
                }
                else if (radioMultiply.Checked)
                {
                    lblResult.Text = (v1 * v2).ToString();
                }
                else if (radioDivide.Checked)
                {
                    lblResult.Text = (v1 / v2).ToString();
                }

            }
            else
            {
                MessageBox.Show("The numbers you entered are invalid!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtValue1_TextChanged(object sender, EventArgs e)
        {
            double i;
            if(double.TryParse(txtValue1.Text, out i))
            {
                txtValue1.BackColor = Color.White;
            }
            else
            {
                txtValue1.BackColor = Color.Pink;
            }
        }

        private void txtValue2_TextChanged(object sender, EventArgs e)
        {
            double i;
            if (double.TryParse(txtValue2.Text, out i))
            {
                txtValue2.BackColor = Color.White;
            }
            else
            {
                txtValue2.BackColor = Color.Pink;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            radioAdd.Checked = true;
        }

        private void subtractToolStripMenuItem_Click(object sender, EventArgs e)
        {
            radioSubtract.Checked = true;
        }

        private void multiplyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            radioMultiply.Checked = true;
        }

        private void divideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            radioDivide.Checked = true;
        }

        private void newWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.Show();
        }
    }
}
