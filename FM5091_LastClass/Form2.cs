﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace FM5091_LastClass
{
    public partial class Form2 : Form
    {
        Graphics gfx;

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            gfx = this.CreateGraphics();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Pen p = new Pen(Color.Blue);
            gfx.DrawLine(p, 50, 50, 250, 250);
        }
    }
}
